//
//  MockCLLocation.swift
//  ContactTracing
//
//  Created by Juan Pablo Rodriguez Medina on 23/07/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation
import CoreLocation

class MockCLFloor: CLFloor {
    override var level: Int {
        return 3
    }
}

class MockLocation: CLLocation {
    override var floor: CLFloor? {
        return MockCLFloor()
    }
    
    override var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: 42.36798272400924, longitude: -83.08552175811849)
    }
}
