//
//  ContactTracing.swift
//  ContactTracing
//
//  Created by Juan Pablo Rodriguez Medina on 04/07/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import UIKit

public class ContactTracing {
    
    public static var shared = ContactTracing()
    
    public var userId: String!
    public var baseUrl: String!
    
    public func presentDebugConsole(from controller: UIViewController) {
        let bundle = Bundle(identifier: "com.navv-systems.ContactTracing")
        let debugConsoleVC = UIStoryboard(name: "Main", bundle: bundle).instantiateViewController(withIdentifier: "BeaconsLogsTabViewController")
        controller.present(debugConsoleVC, animated: true, completion: nil)
    }
    
    public func setBaseUrl(_ url: String) {
        self.baseUrl = url
    }
    
    @available(iOS 13.0, *)
    public func initiateUnitDetection(levelData: Data, unitData: Data, completion: @escaping () -> Void) {
        let imdfDecoder = IMDFDecoder()
        imdfDecoder.decode(level: levelData, unit: unitData, completion: { trees in
            BeaconTracingManager.shared.unitTrees = trees
            completion()
        })
    }
}
