//
//  BeaconTracingManager.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 25/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit
import UserNotifications
import UIKit

let responseProd = """
 [{"UUID":"F261A45A-49DE-4F2E-9BAA-B791DD9BDE73","major":100,"minor":1,"IsReadyToPair":true,"IsPaired":true,"userId":"5e7393e64c525200170d9eca","id":"5eb04c9f25e1e4001ef7dd8f"},{"UUID":"F7826DA6-4FA2-4E98-8024-BC5B71E0893E","major":57902,"minor":15126,"IsReadyToPair":true,"IsPaired":true,"userId":"5c89bcfbaec6d8001797d878","id":"5eb3ea9766094d001e133506"},{"UUID":"F7826DA6-4FA2-4E98-8024-BC5B71E0893E","major":57996,"minor":57835,"IsReadyToPair":true,"IsPaired":true,"userId":"5bb652239d3cea001db19c58","id":"5eb3eb8266094d001e133508"},{"UUID":"F261A45A-49DE-4F2E-9BAA-B791DD9BDE73","major":100,"minor":2,"IsReadyToPair":true,"IsPaired":true,"userId":"5f9c4001d8a3d90019d15657","id":"5ee186e458d0b0001ecea55b"},{"UUID":"70cbcd77-1d4e-443b-8d85-a51585f1b0cf","major":35987,"minor":5355,"IsReadyToPair":true,"IsPaired":true,"userId":"5bec566d517f87001fea8106","id":"5f1797b4bf4d63001e979cec"},{"UUID":"70cbcd77-1d4e-443b-8d85-a51585f1b0cf","major":44184,"minor":65109,"IsReadyToPair":true,"IsPaired":true,"userId":"5c89bda0aec6d8001797e5ec","id":"5f181dc0bf4d63001e97a901"},{"UUID":"70cbcd77-1d4e-443b-8d85-a51585f1b0cf","major":33290,"minor":6863,"IsReadyToPair":true,"IsPaired":true,"userId":"5c89be01aec6d8001797edb8","id":"5f181f3fbf4d63001e97a902"},{"UUID":"70cbcd77-1d4e-443b-8d85-a51585f1b0cf","major":63564,"minor":18834,"IsReadyToPair":true,"IsPaired":true,"userId":"5cc1f08476b3710021e5e855","id":"5f182099bf4d63001e97a903"},{"UUID":"70cbcd77-1d4e-443b-8d85-a51585f1b0cf","major":7492,"minor":29816,"IsReadyToPair":true,"IsPaired":true,"userId":"5e31890dba5dd7001a3a2ae5","id":"5f18226fbf4d63001e97a904"},{"UUID":"F7826DA6-4FA2-4E98-8024-BC5B71E0893E","major":60792,"minor":131,"IsReadyToPair":true,"IsPaired":true,"userId":"5eac585725e1e4001ef4f7c2","id":"5f19f9dfbf4d63001e985187"},{"UUID":"B9407F30-F5F8-466E-AFF9-25556B57FE6D","major":50450,"minor":9463,"IsReadyToPair":true,"IsPaired":false,"userId":"5f9c4028d8a3d90019d15658","id":"5f7fc90eb70434002178401c"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":19272,"minor":44893,"IsReadyToPair":true,"IsPaired":true,"userId":"5ba0fcf1bd1594001d9c2648","id":"60185cfa572ed709a835636d"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":24574,"minor":53399,"IsReadyToPair":true,"IsPaired":true,"userId":"5b9c0a58bd1594001d9c2419","id":"60186e7f572ed709a835636e"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":40480,"minor":13630,"IsReadyToPair":true,"IsPaired":true,"userId":"5b9c11dcbd1594001d9c2433","id":"60186fa4572ed709a835636f"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":19083,"minor":43091,"IsReadyToPair":true,"IsPaired":true,"userId":"5b9c176abd1594001d9c2447","id":"601870b0572ed709a8356370"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":25608,"minor":36936,"IsReadyToPair":true,"IsPaired":true,"userId":"5ba1569dbd1594001d9c2715","id":"601871b7572ed709a8356371"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":29280,"minor":4369,"IsReadyToPair":true,"IsPaired":true,"userId":"5e4ae76bcb6f58001f1cc2b8","id":"601872f0572ed709a8356372"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":60164,"minor":17782,"IsReadyToPair":true,"IsPaired":true,"userId":"5f31a9ff2630f200182c22cd","id":"601873a0572ed709a8356373"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":4832,"minor":59566,"IsReadyToPair":true,"IsPaired":true,"userId":"5fbd508874c22c001bcdc4b4","id":"6018749a572ed709a8356374"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":51553,"minor":37344,"IsReadyToPair":true,"IsPaired":true,"userId":"5fbd57fc74c22c001bcf6929","id":"6018753b572ed709a8356375"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":32204,"minor":7023,"IsReadyToPair":true,"IsPaired":true,"userId":"5fd12e0731c598001a7ebe43","id":"601875db572ed709a8356376"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":26167,"minor":12087,"IsReadyToPair":true,"IsPaired":true,"userId":"5fd1304031c598001a7f364d","id":"601876a1572ed709a8356377"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":13836,"minor":63805,"IsReadyToPair":true,"IsPaired":true,"userId":"5fd1359631c598001a803953","id":"60187bcd572ed709a8356379"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":39700,"minor":12084,"IsReadyToPair":true,"IsPaired":true,"userId":"5fde3289b48291001ad9b368","id":"60187cb0572ed709a835637a"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":14267,"minor":50329,"IsReadyToPair":true,"IsPaired":true,"userId":"6040ffcebad7e5001e48b186","id":"60455047ee3e940d95a42229"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":34059,"minor":50329,"IsReadyToPair":true,"IsPaired":true,"userId":"6045351fbad7e5001e674c7d","id":"60455089ee3e940d95a4222c"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":10212,"minor":61403,"IsReadyToPair":true,"IsPaired":true,"userId":"60453536bad7e5001e674cee","id":"604550adee3e940d95a4222d"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":43072,"minor":50677,"IsReadyToPair":true,"IsPaired":true,"userId":"6045361ebad7e5001e6750ff","id":"604550ddee3e940d95a4222f"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":23154,"minor":1525,"IsReadyToPair":true,"IsPaired":true,"userId":"60453631bad7e5001e67515a","id":"60455115ee3e940d95a42230"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":27264,"minor":13678,"IsReadyToPair":true,"IsPaired":true,"userId":"60453649bad7e5001e6751d8","id":"6045515dee3e940d95a42233"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":9775,"minor":26874,"IsReadyToPair":true,"IsPaired":true,"userId":"60453665bad7e5001e67527b","id":"6045518aee3e940d95a42235"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":10699,"minor":43390,"IsReadyToPair":true,"IsPaired":true,"userId":"6045370fbad7e5001e675654","id":"604551d5ee3e940d95a42237"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":16793,"minor":15435,"IsReadyToPair":true,"IsPaired":true,"userId":"6045378fbad7e5001e67593f","id":"604551f7ee3e940d95a42239"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":7009,"minor":27287,"IsReadyToPair":true,"IsPaired":true,"userId":"604537c2bad7e5001e6759da","id":"6046532bee3e940d95a4223c"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":7509,"minor":50853,"IsReadyToPair":true,"IsPaired":true,"userId":"604537f6bad7e5001e675a5f","id":"6046535dee3e940d95a4223e"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":58486,"minor":26208,"IsReadyToPair":true,"IsPaired":true,"userId":"60453812bad7e5001e675aa7","id":"604653f6ee3e940d95a42240"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":34640,"minor":25503,"IsReadyToPair":true,"IsPaired":true,"userId":"60453846bad7e5001e675b46","id":"60465464ee3e940d95a42242"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":52295,"minor":55927,"IsReadyToPair":true,"IsPaired":true,"userId":"6045386fbad7e5001e675bc2","id":"604654e3ee3e940d95a42245"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":24916,"minor":11782,"IsReadyToPair":true,"IsPaired":true,"userId":"604539c6bad7e5001e67601e","id":"604655f7ee3e940d95a42247"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":32204,"minor":7023,"IsReadyToPair":true,"IsPaired":true,"userId":"604539edbad7e5001e6760ac","id":"60465631ee3e940d95a42248"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":6133,"minor":2300,"IsReadyToPair":true,"IsPaired":true,"userId":"5e5035b287187a001f5c933a","id":"607db80bca3291accb300550"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":52266,"minor":62601,"IsReadyToPair":true,"IsPaired":true,"userId":"5e50367787187a001f5c93f6","id":"607dbb2aca3291accb300552"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":41471,"minor":63100,"IsReadyToPair":true,"IsPaired":true,"userId":"5e501fcb87187a001f5c8026","id":"607dbbc1ca3291accb300553"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":44459,"minor":13394,"IsReadyToPair":true,"IsPaired":true,"userId":"5e501fcb87187a001f5c8026","id":"607dbc39ca3291accb300554"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":43055,"minor":36490,"IsReadyToPair":true,"IsPaired":true,"userId":"5d94f30dd21bce0020d7edea","id":"607dbdd2ca3291accb300556"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":60659,"minor":17612,"IsReadyToPair":true,"IsPaired":true,"userId":"5eaccedcb8c5360020240c18","id":"607dbe98ca3291accb300558"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":55859,"minor":64145,"IsReadyToPair":true,"IsPaired":true,"userId":"5e261028fcfbc0001837941b","id":"607dbf59ca3291accb300559"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":2297,"minor":50393,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85c5b0219ab00017334fe7","id":"607dc094ca3291accb30055b"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":2177,"minor":27012,"IsReadyToPair":true,"IsPaired":true,"userId":"5bc9c9f178e4940021ebf5f8","id":"607de2f2ca3291accb30055d"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":23044,"minor":59833,"IsReadyToPair":true,"IsPaired":true,"userId":"5eae0f44b8c5360020240e5e","id":"607ef731ca3291accb30055e"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":23068,"minor":6689,"IsReadyToPair":true,"IsPaired":true,"userId":"5cd2e8a4a95109001d31cdb2","id":"607efb59ca3291accb30055f"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":31645,"minor":27344,"IsReadyToPair":true,"IsPaired":true,"userId":"5eb457a4b8c5360020247410","id":"607efc9dca3291accb300560"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":19879,"minor":24862,"IsReadyToPair":true,"IsPaired":true,"userId":"5c89524caec6d800178df627","id":"607f1900ca3291accb300561"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":58327,"minor":59023,"IsReadyToPair":true,"IsPaired":true,"userId":"5d963e48d21bce0020f172d7","id":"607f19e8ca3291accb300562"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":46600,"minor":5175,"IsReadyToPair":true,"IsPaired":true,"userId":"5e2616dcfcfbc00018379432","id":"60897c9cd62eb9a0662bcc23"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":53960,"minor":53451,"IsReadyToPair":true,"IsPaired":true,"userId":"5e2611fbfcfbc0001837941f","id":"60897d82d62eb9a0662bcc24"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":10876,"minor":26040,"IsReadyToPair":true,"IsPaired":true,"userId":"5eac2780b8c536002023fa1d","id":"60897de8d62eb9a0662bcc25"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":48325,"minor":16259,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85bae8219ab00017334fd6","id":"60898078d62eb9a0662bcc27"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":62929,"minor":53679,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85cd9a219ab00017334ff2","id":"60898169d62eb9a0662bcc28"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":15846,"minor":15104,"IsReadyToPair":true,"IsPaired":true,"userId":"5eb42c58b8c53600202473e9","id":"608982cbd62eb9a0662bcc29"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":59931,"minor":24888,"IsReadyToPair":true,"IsPaired":true,"userId":"5d9636ffd21bce0020f0d943","id":"60898331d62eb9a0662bcc2a"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":403,"minor":24929,"IsReadyToPair":true,"IsPaired":true,"userId":"5e7d5f94a1bc0a00205b18b9","id":"6092ac9b587bb39cdd3f4a5f"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":29528,"minor":55383,"IsReadyToPair":true,"IsPaired":true,"userId":"5d96365cd21bce0020f0c89f","id":"6092ad56587bb39cdd3f4a60"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":53966,"minor":63741,"IsReadyToPair":true,"IsPaired":true,"userId":"5ba15406bd1594001d9c270e","id":"6092adcd587bb39cdd3f4a61"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":51931,"minor":41957,"IsReadyToPair":true,"IsPaired":true,"userId":"5e5045f787187a001f5ca358","id":"6092ae7a587bb39cdd3f4a62"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":61153,"minor":6761,"IsReadyToPair":true,"IsPaired":true,"userId":"5eb953d5b8c5360020254d5e","id":"6092afcc587bb39cdd3f4a63"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":5664,"minor":17488,"IsReadyToPair":true,"IsPaired":true,"userId":"5e26151afcfbc0001837942d","id":"6092b099587bb39cdd3f4a64"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":6376,"minor":35082,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85c2fd219ab00017334fe3","id":"6092b119587bb39cdd3f4a65"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":20545,"minor":5125,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85bf76219ab00017334fdd","id":"6092b1d0587bb39cdd3f4a66"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":60333,"minor":58864,"IsReadyToPair":true,"IsPaired":true,"userId":"5e50476887187a001f5ca35a","id":"6092b264587bb39cdd3f4a67"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":21499,"minor":41020,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85d5aa219ab00017334ffe","id":"6092b306587bb39cdd3f4a68"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":24313,"minor":63057,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85bb6d219ab00017334fd7","id":"6092b3a1587bb39cdd3f4a69"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":36596,"minor":30425,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85b8b2219ab00017334fd3","id":"6092b446587bb39cdd3f4a6a"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":34127,"minor":1164,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85c17e219ab00017334fe0","id":"60958a1e7af9a9dbc9fcefc3"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":27136,"minor":40246,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85c8a2219ab00017334feb","id":"60958acf7af9a9dbc9fcefc4"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":35578,"minor":48892,"IsReadyToPair":true,"IsPaired":true,"userId":"5c406acbce09e3001dfe600e","id":"60958b6d7af9a9dbc9fcefc5"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":53779,"minor":18795,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85d1a2219ab00017334ff9","id":"60958c1c7af9a9dbc9fcefc6"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":63186,"minor":684,"IsReadyToPair":true,"IsPaired":true,"userId":"5c7d76aeed5a780020b14a11","id":"60958cab7af9a9dbc9fcefc7"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":42772,"minor":40143,"IsReadyToPair":true,"IsPaired":true,"userId":"5b85cc64219ab00017334ff0","id":"60958d027af9a9dbc9fcefc8"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":35557,"minor":11128,"IsReadyToPair":true,"IsPaired":true,"userId":"5c406f65ce09e3001dfe6a20","id":"609594827af9a9dbc9fcefca"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":6814,"minor":5832,"IsReadyToPair":true,"IsPaired":true,"userId":"5d963dacd21bce0020f16c3e","id":"6095969f7af9a9dbc9fcefcc"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":35995,"minor":11128,"IsReadyToPair":true,"IsPaired":true,"userId":"5d94f201d21bce0020d7df1e","id":"609597207af9a9dbc9fcefcd"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":454,"minor":21971,"IsReadyToPair":true,"IsPaired":true,"userId":"5eae1aacb8c5360020240e60","id":"609597737af9a9dbc9fcefce"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":23757,"minor":6314,"IsReadyToPair":true,"IsPaired":true,"userId":"5eae1b60b8c5360020240e61","id":"609597f17af9a9dbc9fcefcf"},{"UUID":"f7826da6-4fa2-4e98-8024-bc5b71e0893e","major":56591,"minor":64020,"IsReadyToPair":true,"IsPaired":true,"userId":"5d962d3bd21bce0020efd4ff","id":"609598957af9a9dbc9fcefd1"}]
"""

class Alerts {
    static func displayAlert(with title: String, and message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        //...
        var rootViewController = UIApplication.shared.keyWindow?.rootViewController
        if let navigationController = rootViewController as? UINavigationController {
            rootViewController = navigationController.viewControllers.first
        }
        if let tabBarController = rootViewController as? UITabBarController {
            rootViewController = tabBarController.selectedViewController
        }
        //...
        rootViewController?.present(alertController, animated: true, completion: nil)
    }
}

public class BeaconTracingManager:NSObject {
    
    let silenceNotificationsCategoryId = "SILENCE_NOTIFICATIONS"
    let contactNotificationId = "CONTACT_NORTIFICATION"
    let badgeAwayNotification = "BADGE_AWAY_NOTIFICATION"
    let userIdKey = "USER_ID"
    public let muteActionId = "SILENCE_USER"
    
    public typealias RangeCallback = ([CLBeacon]) -> Void
    
    public static let shared = BeaconTracingManager()
    var regions:[Region]?
    var beacons:[BeaconId:BeaconStatus]?
    var logs:[ContactLog]?
    var didFinishGettingBeacons:(() -> Void)?
    var didRangeBeaconsCallbacks = [CallbackWrapper<RangeCallback>]()
    var didStartRangingRegion:((_ uuid:String, _ index:Int) -> Void)?
    var didStopRangingRegion:((_ uuid:String, _ index:Int) -> Void)?
    var didStartMonitoringRegions:(() -> Void)?
    var didStopMonitoringRegions:(() -> Void)?
    var didLogContact:(() -> Void)?
    var isMonitoring:Bool = false
    var badgeLogTimeTiers:[Double]
    var equipmentLogTimeTiers:[Double] = [0]
    var mutedUsers:Set<String>?
    var myBadge:BadgeBeacon?
    
    enum TracingType:String {
        case equipment
        case badge
        
        var distanceThreshold:Double {
            switch self {
                case .equipment:
                    return 2.0
                case .badge:
                    return 2.0
            }
        }
        
        var resetDistanceThreshold:Double {
            switch self {
                case .equipment:
                    return 10.0
                case .badge:
                    return 10.0
            }
        }
        
        var resetTimeThreshold:Double {
            switch self {
                case .equipment:
                    return 0.0
                case .badge:
                    return 60 * 10
            }
        }
    }
    
    class BeaconStatus {
        var beacon:Beacon
        var nextLoggingLevel:Int = 0
        var tracingType:TracingType
        var firstContactDate:Date?
        
        init(beacon:Beacon, type:TracingType) {
            self.beacon = beacon
            self.tracingType = type
        }
    }
    
    private var locationManager:CLLocationManager?
    private var currentLocation:CLLocation?
    private let ownBadgeDistanceThreshold:Double = 10.0
    private let ownBadgeTimeThreshold:Double = 30.0
    private let resetDistanceThreshold:Double = 3.0
    
    private var rangingQueue = [String]()
    private var minRangingTime:Double = 2.0
    private var maxRangingTime:Double = 5.0
    private var roundRobinTimer:Timer?
    private var currentRangedRegions = Set<String>()
    var isRoundRobinEnabled:Bool {
        get {
            return UserDefaults.standard.bool(forKey: UserDefaultsKeys.isRoundRobinEnabled.rawValue)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.isRoundRobinEnabled.rawValue)
            if newValue {
                for region in self.regions ?? [] {
                    self.stopRangingRegion(region.UUID)
                }
                self.roundRobin()
            }
            else {
                for region in self.currentRangedRegions {
                    self.stopRangingRegion(region)
                }
                for region in self.regions ?? [] {
                    self.startRangingRegion(region.UUID)
                }
            }
        }
    }
    var maxSimultaneousRangingCount:Int {
        get {
            let val = UserDefaults.standard.integer(forKey: UserDefaultsKeys.roundRobinMaxSimultaneousRangingCount.rawValue)
            return val == 0 ? 2 : val
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.roundRobinMaxSimultaneousRangingCount.rawValue)
        }
    }
    var roundRobinTime:Double {
        get {
            let val = UserDefaults.standard.double(forKey: UserDefaultsKeys.roundRobinTime.rawValue)
            return val == 0 ? 40 : Double(val)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKeys.roundRobinTime.rawValue)
        }
    }
    public var shouldDisplayContactAlerts:Bool = true
    
    var unitTrees: [Int? : Any]?
    
    override init() {
        
        //Get badge log tiers
       // self.badgeLogTimeTiers = UserDefaults.standard.array(forKey: UserDefaultsKeys.badgeLogTimeTiers.rawValue) ?? [60, 120, 180]
        self.badgeLogTimeTiers = UserDefaults.standard.array(forKey: UserDefaultsKeys.badgeLogTimeTiers.rawValue) ?? [600, 900, 1200]
        super.init()
        
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        self.locationManager?.startUpdatingLocation()
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager?.allowsBackgroundLocationUpdates = true
        self.locationManager?.pausesLocationUpdatesAutomatically = false
        self.logs = [ContactLog]()
        
        //Get stored muted users
        self.mutedUsers = UserDefaults.standard.set(forKey: UserDefaultsKeys.mutedContactUsers.rawValue) ?? []
       
        //Application will terminate notification
        NotificationCenter.default.addObserver(forName: UIApplication.willTerminateNotification, object: nil, queue: .main) { [weak self] _ in
            if let mutedUsers = self?.mutedUsers {
                print("Saving muted users")
                UserDefaults.standard.set(value: mutedUsers, forKey: UserDefaultsKeys.mutedContactUsers.rawValue)
                UserDefaults.standard.setValue(self?.badgeLogTimeTiers, forKey: UserDefaultsKeys.badgeLogTimeTiers.rawValue)
            }
        }
    }
    
    private func getEquipmentBeacons(_ completion: @escaping ([Beacon]?) -> Void) {
        
        let url =  ContactTracing.shared.baseUrl + Constants.Urls.EQUIPMENT
        
        NetworkingHelper.shared.request(url: url, method: .get, headers: nil, body: nil, retryWaitingTime: 1.5, maxRetryAttempts: 5) { (data, response, error) in
            completion(data?.toObject(type: [EquipmentBeacon].self))
        }
    }
    
    private func getBadgesBeacons(_ completion: @escaping ([Beacon]?) -> Void) {
        let url =  ContactTracing.shared.baseUrl + Constants.Urls.BADGES
        NetworkingHelper.shared.request(url: url, method: .get, headers: nil, body: nil, retryWaitingTime: 1, maxRetryAttempts: 5) { (data, response, error) in
        
//            do {
//                if let JSONData = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments, .fragmentsAllowed])  as? [[String: Any]] {
//
//                    let jsonData = try JSONSerialization.data(withJSONObject: JSONData, options: [.fragmentsAllowed, .prettyPrinted])
//                    let badges = try JSONDecoder().decode([BadgeBeacon].self, from: jsonData)
//                       completion(badges)
//
//                } else {
//                    completion(nil)
//                }
//            } catch let error as NSError {
//                print("Failed to load: \(error.localizedDescription)")
//                completion(nil)
//
//            }
            
            completion(data?.toObject(type: [BadgeBeacon].self))
//
//            let data = responseProd.data(using: .utf8)!
//            do {
//                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
//                {
//                    let jsonData = try JSONSerialization.data(withJSONObject: jsonArray, options: [])
//
//                        let badges = try JSONDecoder().decode([BadgeBeacon].self, from: jsonData)
//                    print(badges)
//
//                } else {
//                    print("bad json")
//                }
//            } catch let error as NSError {
//                print(error)
//            }

        }
    }
    
    private func logContact(beaconId:BeaconId, location:CLLocation, accuracy:Double, date:Date, time:Double, completion: @escaping (Bool) -> Void) {
        
        let type = self.beacons?[beaconId]?.tracingType
        
        if type == .equipment {
            let unitId = self.findEnclosingUnit(location: location) ?? "Unknown"
            let roundedAccuracy = round(accuracy * 1000) / 1000
            let record = EquipmentRecord(UUID: beaconId.UUID, major: beaconId.major, minor: beaconId.minor, location: location.coordinate, floor: location.floor?.level ?? 0, accuracy: roundedAccuracy, unitId: unitId, dateTime: date, deviceId: ContactTracing.shared.userId ?? "Unknown")
            let body = try? record.jsonData()
            NetworkingHelper.shared.request(url: ContactTracing.shared.baseUrl + Constants.Urls.EQUIPMENT_LOG, method: .post, headers: ["Content-Type":"application/json"], body: body, retryWaitingTime: 10) { (_, response, _) in
                completion(response?.statusCode == 200)
            }
        }
        else if type == .badge {
            if let beacon = self.beacons?[beaconId]?.beacon as? BadgeBeacon,
                let userId = beacon.userId,
            userId != ContactTracing.shared.userId {
                
                self.notifyBadgeContact(beacon: beacon, time: time)
                
                let badgeRecord = BadgeRecord(id: nil, userId: ContactTracing.shared.userId!, dateTime: date, location: location.coordinate, floor: location.floor?.level, contactUserId: userId, contactTime: Int(time))
                let body = try? badgeRecord.jsonData()
                NetworkingHelper.shared.request(url: ContactTracing.shared.baseUrl + Constants.Urls.BADGE_LOG, method: .post, headers: ["Content-Type":"application/json"], body: body, retryWaitingTime: 10) { (_, response, _) in
                    completion(response?.statusCode == 200)
                }
            }
            else {
                completion(false)
            }
        }
    }
    
    private func notifyBadgeContact(beacon:BadgeBeacon, time:Double) {
        if let userId = beacon.userId,
        !(self.mutedUsers?.contains(userId) ?? false),
        self.shouldDisplayContactAlerts {
            let userName = userId
            let timeInMin = Int(time / 60.0)
            let distance = Int(TracingType.badge.distanceThreshold)
            let content = UNMutableNotificationContent()
            content.title = "Contact Alert"
            content.body = "You have been in contact with \(userName) for at least \(timeInMin) minute\(timeInMin > 1 ? "s" : "") and within a distance of \(distance) meter\(distance > 1 ? "s" : "")."
            content.userInfo = [self.userIdKey:userId]
            content.sound = .default
            content.categoryIdentifier = self.silenceNotificationsCategoryId
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false)
            let request = UNNotificationRequest(identifier: self.contactNotificationId, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            print("🟦 - Sending contact notification \(beacon.beaconId)")
        }
    }
    
    private func notifyOwnBadgeAway() {
        if self.shouldDisplayContactAlerts {
            let content = UNMutableNotificationContent()
            content.title = "Badge Away"
            content.body = "Your badge seems to be more than \(String(format:"%.0f", self.ownBadgeDistanceThreshold)) meters away from your device"
            content.sound = .default
            let request = UNNotificationRequest(identifier: self.badgeAwayNotification, content: content, trigger: nil)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            print("🟦 - Own badge away")
        }
    }
    
    private func setRegions(forBeacons beacons:[Beacon], tracingType:TracingType) {
        for beacon in beacons {
            self.beacons?[beacon.beaconId] = BeaconStatus(beacon: beacon, type: tracingType)
            
            if let regionIndex = self.regions?.firstIndex(where: { $0.UUID == beacon.beaconId.UUID }) {
                self.regions?[regionIndex].beacons.append(beacon)
            }
            else {
                let region = Region(uuid: beacon.beaconId.UUID)
                region.beacons.append(beacon)
                self.regions?.append(region)
            }
        }
    }
    
    private func processContact(clbeacon:CLBeacon) {
        
        let beaconId = BeaconId(UUID: clbeacon.uuid.uuidString, major: clbeacon.major.intValue, minor: clbeacon.minor.intValue)
        
        if let beaconStatus = self.beacons?[beaconId] {
            let accuracy = clbeacon.accuracy
            let now = Date()
            
            if let userId = (beaconStatus.beacon as? BadgeBeacon)?.userId,
                userId == ContactTracing.shared.userId {
                self.myBadge = beaconStatus.beacon as? BadgeBeacon
                if accuracy > self.ownBadgeDistanceThreshold {
                    if beaconStatus.firstContactDate == nil {
                        beaconStatus.firstContactDate = now
                    } else if beaconStatus.nextLoggingLevel == 0 {
                        let elapsedTime = now.timeIntervalSince(beaconStatus.firstContactDate!)
                        if elapsedTime > self.ownBadgeTimeThreshold {
                            beaconStatus.nextLoggingLevel = 1
                            self.notifyOwnBadgeAway()
                        }
                    }
                }
                else if accuracy > 0 && accuracy <= self.resetDistanceThreshold {
                    beaconStatus.firstContactDate = nil
                    beaconStatus.nextLoggingLevel = 0
                }
            }
            else {
                if accuracy > 0 && accuracy <= beaconStatus.tracingType.distanceThreshold {
                    
                    if beaconStatus.firstContactDate == nil {
                        
                        var firstContactDate = now
                        
                        if let lastContact = self.logs?.filter({ $0.beaconId == beaconStatus.beacon.beaconId }).sorted(by: { $0.date > $1.date }).first {
                            
                            let elapsedTime = now.timeIntervalSince(lastContact.date)
                            
                            let diff = beaconStatus.tracingType.resetTimeThreshold - elapsedTime
                            
                            if diff > 0  {
                                firstContactDate = Calendar.current.date(byAdding: .second, value: Int(diff), to: now) ?? now
                            }
                        }
                        
                        beaconStatus.firstContactDate = firstContactDate
                        
                        print("🟦 - Did set first contact date for beacon: \(beaconId) \(now)")
                    }
                    
                    let shouldLog = self.shouldLog(beaconId: beaconId, date: now)
                    if shouldLog.should,
                        let location = self.currentLocation {
                        beaconStatus.nextLoggingLevel = shouldLog.logLevel + 1
                        self.logContact(beaconId: beaconId, location: location, accuracy: accuracy, date: now, time: shouldLog.time) { [weak self] (success) in
                            if success {
                                //Add log to debug logs
                                let log = ContactLog(beaconId: beaconId, date: now, time: shouldLog.time, coordinate:location.coordinate, floor: location.floor?.level, uploaded: success)
                                self?.logs?.append(log)
                                self?.didLogContact?()
                                print("🟦 - Logged beacon \(beaconId) \(now)")
                            }
                            else {
                                beaconStatus.nextLoggingLevel -= 1
                            }
                        }
                    }
                    
                } else if accuracy >= beaconStatus.tracingType.resetDistanceThreshold {
                    beaconStatus.firstContactDate = nil
                    beaconStatus.nextLoggingLevel = 0
                    print("🟦 - Did set beacon should log as true \(beaconId)")
                }
            }
        }
    }
    
    private func shouldLog(beaconId:BeaconId, date:Date) -> (should:Bool, time:Double, logLevel:Int) {
        if let status = self.beacons?[beaconId] {
            
            let tiers = status.tracingType == .badge ? self.badgeLogTimeTiers : self.equipmentLogTimeTiers
            guard status.nextLoggingLevel < tiers.count else {
                return (false, 0, tiers.count)
            }
            
            let now = date
            let elapsedTime = now.timeIntervalSince(status.firstContactDate ?? now)
            
            var loggingLevel = status.nextLoggingLevel
            for i in status.nextLoggingLevel..<tiers.count {
                if elapsedTime >= tiers[i] {
                    loggingLevel = i
                }
                else {
                    break
                }
            }
            let nextLogTier = tiers[safe:loggingLevel] ?? tiers[tiers.count - 1]
            
            return (elapsedTime >= nextLogTier, nextLogTier, loggingLevel)
        }
        return (false, 0, 0)
    }
    
    public func silenceNotificationAction() -> UNNotificationCategory {
        let action = UNNotificationAction(identifier: self.muteActionId, title: "Silence future notifications for this user", options: UNNotificationActionOptions(rawValue: 0))
        let category = UNNotificationCategory(identifier: self.silenceNotificationsCategoryId, actions: [action], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "", options: .customDismissAction)
        return category
    }
    
    public func handleSilenceNotification(response: UNNotificationResponse) {
        if let userId = response.notification.request.content.userInfo[self.userIdKey] as? String {
            self.mutedUsers?.insert(userId)
        }
    }
    
    public func subscribeToDidRangeBeacons(callback:CallbackWrapper<RangeCallback>?) {
        if let callback = callback {
            self.didRangeBeaconsCallbacks.append(callback)
        }
    }
    
    public func unsubscribeFromDidRangeBeacons(callback:CallbackWrapper<RangeCallback>?) {
        if let callback = callback {
            self.didRangeBeaconsCallbacks.removeAll(where: {$0.id == callback.id})
        }
    }
    
    public func getBeacons(shouldMonitor:Bool, completion:((Bool) -> Void)? = nil) {
        self.isMonitoring = false
        self.regions = [Region]()
        self.beacons = [BeaconId:BeaconStatus]()
        
        let group = DispatchGroup()
        
        group.enter()
        self.getEquipmentBeacons { (beacons) in
            if let beacons = beacons {
                self.setRegions(forBeacons: beacons, tracingType: .equipment)
            }
            group.leave()
        }
        
        group.enter()
        self.getBadgesBeacons { (beacons) in
            if let beacons = beacons {
                self.setRegions(forBeacons: beacons, tracingType: .badge)
            }
            group.leave()
        }
        
        group.notify(queue: .main) {[weak self] in
            let success = (self?.regions?.count ?? 0) > 0
            
            if success {
                self?.didFinishGettingBeacons?()
                if shouldMonitor {
                    self?.startMonitoring()
                }
            }
            completion?(success)
        }
    }
    
    func updateRequest(forBadge badge:BadgeBeacon, completion: @escaping (BadgeBeacon?) -> Void) {
        let body = try? badge.jsonData()
        NetworkingHelper.shared.request(url: ContactTracing.shared.baseUrl + Constants.Urls.BADGES, method: .put, headers: ["Content-Type":"application/json"], body: body, retryWaitingTime: 1, maxRetryAttempts: 5) { (data, response, _) in
            completion(badge)
        }
    }
    
    func set(badgeId: BeaconId, paired: Bool, completion: ((BadgeBeacon?) -> Void)?) {
        if let badge = self.beacons?[badgeId]?.beacon as? BadgeBeacon {
            badge.isPaired = paired
            badge.userId = paired ? ContactTracing.shared.userId : nil
            if !paired { self.myBadge = nil }
            
            self.updateRequest(forBadge: badge) { [weak self] (updatedBadge) in
                if let updatedBadge = updatedBadge {
                    self?.beacons?[updatedBadge.beaconId]?.beacon = updatedBadge
                    if paired { self?.myBadge = updatedBadge }
                }
                completion?(updatedBadge)
            }
        }
        else {
            completion?(nil)
        }
    }
    
    public func pairBadge(badgeId: BeaconId, completion: ((Bool) -> Void)?) {
        self.set(badgeId: badgeId, paired: true) { updatedBadge in
            completion?(updatedBadge != nil)
        }
    }
    
    public func unpairBadge(badgeId: BeaconId, completion: ((Bool) -> Void)?) {
        self.set(badgeId: badgeId, paired: false) { updatedBadge in
            completion?(updatedBadge != nil)
        }
    }
    
    public func isPairableBadge(beaconId: BeaconId) -> Bool {
        if let badge = self.beacons?[beaconId]?.beacon as? BadgeBeacon {
            return !badge.isPaired && badge.isReadyToPair
        }
        return false
    }
    
    public func findBadge(pairedWith userId:String) -> BeaconId? {
        if let beacons = BeaconTracingManager.shared.beacons?.values {
            for beaconStatus in beacons  {
                if let badge = beaconStatus.beacon as? BadgeBeacon {
                    if badge.userId == userId {
                        return badge.beaconId
                    }
                }
            }
        }
        return nil
    }
    
    public func startMonitoring() {
        if let regions = self.regions {
            for (index, region) in regions.enumerated() {
                if let uuid = UUID(uuidString: region.UUID) {
                    let clRegion = CLBeaconRegion(proximityUUID: uuid, identifier: region.UUID)
                    self.regions?[index].status = .monitored
                    self.locationManager?.startMonitoring(for: clRegion)
                    self.locationManager?.requestState(for: clRegion)
                }
            }
            self.isMonitoring = true
            self.didStartMonitoringRegions?()
        }
    }
    
    public func stopMonitoring() {
        
        if let rangedRegions = self.locationManager?.rangedRegions {
            for region in rangedRegions {
                if let beaconRegion = region as? CLBeaconRegion {
                    self.locationManager?.stopRangingBeacons(in: beaconRegion)
                }
            }
        }
        
        if let monitoredRegions = self.locationManager?.monitoredRegions {
            for region in monitoredRegions {
                if let beaconRegion = region as? CLBeaconRegion {
                    self.locationManager?.stopMonitoring(for: beaconRegion)
                    if let regionIndex  = self.regions?.firstIndex(where: { $0.UUID == beaconRegion.uuid.uuidString }) {
                        self.regions?[regionIndex].status = .notMonitored
                    }
                }
            }
            self.isMonitoring = false
            self.didStopMonitoringRegions?()
        }
    }
    
    public func handleLogout() {
        self.stopMonitoring()
        self.logs = [ContactLog]()
        self.mutedUsers = Set<String>()
        UserDefaults.standard.set(nil, forKey: UserDefaultsKeys.mutedContactUsers.rawValue)
    }
    
    private func roundRobin() {
        if self.isRoundRobinEnabled {
            print("RR - Initiating round-robin")
            let next = self.getNextRangingRegions()
            if let time = self.getRangingTime() {
                print("RR - Starting slot of \(time) seconds")
                print("RR - Current: \(self.currentRangedRegions)")
                print("RR - Next: \(next)")
                if !next.isSubset(of: self.currentRangedRegions) {
                    for region in self.currentRangedRegions {
                        self.stopRangingRegion(region, roundRobin: true)
                    }
                    self.currentRangedRegions = next
                    print("Going to monitor: \(next)")
                    
                    for region in next {
                        self.startRangingRegion(region)
                    }
                }
                
                roundRobinTimer = Timer.scheduledTimer(withTimeInterval: time, repeats: false, block: { [weak self] _ in
                    self?.roundRobin()
                })
            }
        }
    }
    
    private func getRangingTime() -> Double? {
        
        guard self.rangingQueue.count > 0 else {
            return nil
        }
        
        var time = self.roundRobinTime / Double(self.rangingQueue.count)
        time = round(min(max(self.minRangingTime, time), self.maxRangingTime) * 100) / 100
        return time
    }
    
    private func getNextRangingRegions() -> Set<String> {
        if self.rangingQueue.count < self.maxSimultaneousRangingCount {
            return Set(self.rangingQueue)
        }
        let range = 0..<self.maxSimultaneousRangingCount
        let next = self.rangingQueue[range]
        self.rangingQueue.removeSubrange(range)
        self.rangingQueue.append(contentsOf: next)
        
        return Set(next)
    }
    
    private func queueRegion(_ region:String) {
        
        self.rangingQueue.insert(region, at: 0)
        
        if self.isRoundRobinEnabled {
            if self.rangingQueue.count == 1 {
                self.roundRobin()
            }
        }
        else {
            self.startRangingRegion(region)
        }
    }
    
    private func unqueueRegion(_ region:String) {
        if let index = self.rangingQueue.firstIndex(where: { $0 == region}) {
            self.rangingQueue.remove(at: index)
        }
        
        if !self.isRoundRobinEnabled {
            self.stopRangingRegion(region)
        }
    }
    
    private func startRangingRegion(_ regionId:String) {
        print("🟦 - Started ranging beacons in region \(regionId)")
        let region = CLBeaconRegion(proximityUUID: UUID(uuidString: regionId)!, identifier: regionId)
        self.locationManager?.startRangingBeacons(in: region)
        if let regionIndex = self.regions?.firstIndex(where: { $0.UUID == region.proximityUUID.uuidString }) {
            self.regions?[regionIndex].status = .ranging
            self.didStartRangingRegion?(region.uuid.uuidString, regionIndex)
        }
    }
    
    private func stopRangingRegion(_ regionId:String, roundRobin:Bool = false) {
        print("🟦 - Stopped ranging equipment in region \(regionId)")
        let region = CLBeaconRegion(proximityUUID: UUID(uuidString: regionId)!, identifier: regionId)
        self.locationManager?.stopRangingBeacons(in: region)
        if let regionIndex = self.regions?.firstIndex(where: { $0.UUID == region.uuid.uuidString }) {
            self.regions?[regionIndex].status = roundRobin ? .roundRobin : .monitored
            self.didStopRangingRegion?(region.uuid.uuidString, regionIndex)
        }
    }
    
    private func didRangeBeacons(beacons:[CLBeacon]) {
        for callback in self.didRangeBeaconsCallbacks {
            callback.callback(beacons)
        }
    }
    
    public func findEnclosingUnit(location: CLLocation) -> String? {
        
        if #available(iOS 13.0, *),
            let floor = location.floor?.level,
            let tree = self.unitTrees?[floor] as? KDTree<UnitPoint> {
            
            let contactPoint = UnitPoint(identifier: nil, title: nil, category: nil, coordinate: location.coordinate, geometry: nil)
            
            var enclosingUnit = tree.nearest(to: contactPoint, maxDistance: Double.greatestFiniteMagnitude) { unit -> Bool in
                print("Evaluating if enclosing: \(String(describing: unit.title))")
                if let overlay = unit.geometry?[0] as? MKOverlay {
                    let polygonRenderer = MKPolygonRenderer(overlay: overlay)
                    let mapPoint = MKMapPoint(location.coordinate)
                    let polygonViewPoint = polygonRenderer.point(for: mapPoint)
                    return polygonRenderer.path.contains(polygonViewPoint)
                }
                return false
            }
            
            if enclosingUnit?.category == "walkway" {
                enclosingUnit = tree.nearest(to: contactPoint, maxDistance: Double.greatestFiniteMagnitude, where: { unit -> Bool in
                    return unit.category != "walkway"
                })
            }
            
            return enclosingUnit?.title
        }
        return nil
    }
}

extension BeaconTracingManager: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        print("🟦 - Did range equipment beacons: \(beacons)")
        self.didRangeBeacons(beacons: beacons)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didRangeEquipmentBeacons"), object: nil, userInfo: ["beacons": beacons])

        for clbeacon in beacons {
            self.processContact(clbeacon: clbeacon)
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error) {
        print("🟦 - Ranging beacons did fail for region \(region)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("🟦 - Did enter region \(region)")
        if let beaconRegion = region as? CLBeaconRegion {
            self.queueRegion(beaconRegion.uuid.uuidString)
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("🟦 - Did exit region \(region)")
        if let beaconRegion = region as? CLBeaconRegion {
            self.unqueueRegion(beaconRegion.uuid.uuidString)
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("🟦 - Did start monitoring region \(region)")
    }
    
    public func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("🟦 - Did fail monitoring region \(String(describing: region))")
    }
    
    public func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        
        if let beaconRegion = region as? CLBeaconRegion {
            if state == .inside {
                self.queueRegion(beaconRegion.uuid.uuidString)
            } else if state == .outside {
                self.unqueueRegion(beaconRegion.uuid.uuidString)
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("🌍 - Did update locations from Beacons Tracing Manager: \(locations)")
        self.currentLocation = locations.first
    }
}
