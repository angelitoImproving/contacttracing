//
//  CollectionViewGridLayout.swift
//  ContactTracing
//
//  Created by Juan Pablo Rodriguez Medina on 31/07/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import UIKit

class CollectionViewGridLayout: UICollectionViewLayout {
    
    private var rowsCount: Int {
        return self.collectionView!.numberOfSections
    }
    
    private func columnCount(in row: Int) -> Int {
        return self.collectionView!.numberOfItems(inSection: row)
    }
    
    private var contentSize: CGSize = .zero
    
    private var layoutAttributes: [[UICollectionViewLayoutAttributes]] = []
    
    override var collectionViewContentSize: CGSize {
        return self.contentSize
    }
    
    private func setupAttributes() {
        self.layoutAttributes = []
        
        var xOffset: CGFloat = 0.0
        var yOffset: CGFloat = 0.0
        
        for row in 0..<self.rowsCount {
            
            var rowAttrs: [UICollectionViewLayoutAttributes] = []
            xOffset = 0.0
            
            for col in 0..<self.columnCount(in: row) {
                let itemSize = self.size(forRow: row, column: col)
                let indexPath = IndexPath(item: col, section: row)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemSize.width, height: itemSize.height)
                rowAttrs.append(attributes)
                xOffset += itemSize.width
            }
            
            yOffset += rowAttrs.last?.frame.height ?? 0.0
            self.layoutAttributes.append(rowAttrs)
        }
        if let width = self.layoutAttributes.last?.last?.frame.maxX,
            let height = self.layoutAttributes.last?.last?.frame.maxY {
            self.contentSize = CGSize(width: width, height: height)
        }
    }
    
    private func size(forRow row: Int, column: Int) -> CGSize {
        if let delegate = self.collectionView?.delegate as? UICollectionViewDelegateFlowLayout,
            let size = delegate.collectionView?(self.collectionView!, layout: self, sizeForItemAt: IndexPath(item: column, section: row)) {
            return size
        }
        return .zero
    }
    
    override func prepare() {
        if self.layoutAttributes.count != self.collectionView?.numberOfSections {
            self.setupAttributes()
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var attributes = [UICollectionViewLayoutAttributes]()
        
        for section in self.layoutAttributes {
            
            for col in section {
                if rect.intersects(col.frame) {
                    attributes.append(col)
                }
                
                if col.indexPath.item == 0 {
                    var frame = col.frame
                    frame.origin.x =  self.collectionView!.contentOffset.x
                    col.frame = frame
                    col.zIndex = 1
                }
                
                if col.indexPath.section == 0 {
                    var frame = col.frame
                    frame.origin.y =  self.collectionView!.contentOffset.y
                    col.frame = frame
                    col.zIndex = 1
                }
                
                if col.indexPath.section == 0 && col.indexPath.item == 0 {
                    col.zIndex = 2
                }
            }
            
            attributes.append(contentsOf: section.filter { rect.intersects($0.frame) })
        }
        
        return attributes
    }
        
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
