/*
 See LICENSE folder for this sample’s licensing information.
 
 Abstract:
 This class decodes GeoJSON-based IMDF data and returns structured types.
 */

import Foundation
import MapKit

@available(iOS 13.0, *)
protocol IMDFDecodableFeature {
    init(feature: MKGeoJSONFeature) throws
}

enum IMDFError: Error {
    case invalidType
    case invalidData
}

public struct IMDFArchive {
    let baseDirectory: URL
    init(directory: URL) {
        baseDirectory = directory
    }
    
    enum File {
        case level
        case unit
        
        var filename: String {
            return "\(self).geojson"
        }
    }
    
    func fileURL(for file: File) -> URL {
        return baseDirectory.appendingPathComponent(file.filename)
    }
}

@available(iOS 13.0, *)
class IMDFDecoder {
    
    private let geoJSONDecoder = MKGeoJSONDecoder()
    
    func decode(_ imdfDirectory: URL) throws -> [Int? : KDTree<UnitPoint>] {
        let archive = IMDFArchive(directory: imdfDirectory)
        
        do {
            // Decode all the features that need to be rendered.
            let levels = Dictionary(grouping: try decodeFeatures(Level.self, from: .level, in: archive), by: { $0.identifier })
            let units = try decodeFeatures(Unit.self, from: .unit, in: archive)
            
            
            // Associate Units and Opening to levels.
            let unitTreesByOrdinal = Dictionary(grouping: units, by: { levels[$0.properties.levelId]?.first?.properties.ordinal }).mapValues { KDTree<UnitPoint>(values: $0.map { UnitPoint(identifier: $0.identifier, title: $0.properties.altName?.bestLocalizedValue ?? $0.properties.name?.bestLocalizedValue, category: $0.properties.category, coordinate: $0.properties.displayPoint?.coordinates ?? CLLocationCoordinate2D(), geometry: $0.geometry )}) }
            
            return unitTreesByOrdinal
            
        } catch let error {
            throw error
        }
    }
    
    func decode(level levelData: Data, unit unitData: Data, completion: @escaping (_ trees:[Int? : KDTree<UnitPoint>]?) -> Void) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            if let units = try? self.decodeFeatures(Unit.self, in: unitData),
                let levels = try? self.decodeFeatures(Level.self, in: levelData){
                // Decode all the features that need to be rendered.
                let levelsById = Dictionary(grouping: levels, by: { $0.identifier })
                
                // Associate Units and Opening to levels.
                let unitTreesByOrdinal = Dictionary(grouping: units, by: { levelsById[$0.properties.levelId]?.first?.properties.ordinal }).mapValues { KDTree<UnitPoint>(values: $0.map { UnitPoint(identifier: $0.identifier, title: $0.properties.altName?.bestLocalizedValue ?? $0.properties.name?.bestLocalizedValue, category: $0.properties.category, coordinate: $0.properties.displayPoint?.coordinates ?? CLLocationCoordinate2D(), geometry: $0.geometry )}) }
                DispatchQueue.main.async {
                    completion(unitTreesByOrdinal)
                }
            }
            else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
    
    public func decodeFeatures<T: IMDFDecodableFeature>(_ type: T.Type, from file: IMDFArchive.File, in archive: IMDFArchive) throws -> [T] {
        do {
            let fileURL = archive.fileURL(for: file)
            let data = try Data(contentsOf: fileURL)
            return try self.decodeFeatures(T.self, in: data)
            
        } catch let error {
            throw error
        }
    }
    
    public func decodeFeatures<T: IMDFDecodableFeature>(_ type: T.Type, in data: Data) throws -> [T] {
        do {
            let geoJSONFeatures = try geoJSONDecoder.decode(data)
            guard let features = geoJSONFeatures as? [MKGeoJSONFeature] else {
                throw IMDFError.invalidType
            }
            
            let imdfFeatures = try features.map { try type.init(feature: $0) }
            
            return imdfFeatures
            
        } catch let error {
            throw error
        }
    }
}

