//
//  BeaconTVCell.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 25/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class BeaconTVCell: UITableViewCell {
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var majorLabel: UILabel!
    @IBOutlet weak var minorLabel: UILabel!
    @IBOutlet weak var rssiLabel: UILabel!
    @IBOutlet weak var accuracyLabel: UILabel!
}

