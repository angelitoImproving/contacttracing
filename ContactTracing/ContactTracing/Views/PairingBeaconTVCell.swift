//
//  PairingBeaconTVCell.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 29/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class PairingBeaconTVCell: UITableViewCell {
    
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var isPairedSwitch: UISwitch!
    @IBOutlet weak var majorMinorLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    
    var pairStateChangeRequested:((_ sender: PairingBeaconTVCell) -> Void)?
    
    @IBAction func isPairedValueChanged(_ sender: UISwitch) {
        self.pairStateChangeRequested?(self)
    }
}
