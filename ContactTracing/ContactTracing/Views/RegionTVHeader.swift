//
//  RegionTVHeader.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 25/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class RegionTVHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        if #available(iOS 13.0, *) {
            self.containerView.backgroundColor = UIColor.systemGray4
        } else {
            // Fallback on earlier versions
            self.containerView.backgroundColor = UIColor(named: "table-header")
        }
    }
    
    func setStatus(_ status:String, withColor color:UIColor, animating:Bool) {
        self.statusLabel.text = status
        self.statusLabel.textColor = color
    }
}

