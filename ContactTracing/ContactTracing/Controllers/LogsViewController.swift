//
//  LogsViewController.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 25/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class LogsViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    
    private static let dateFormatter:DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Dismiss button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissController(sender:)))
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        BeaconTracingManager.shared.didLogContact = { [weak self] in
            if UIApplication.shared.applicationState == .active && self?.viewIfLoaded?.window != nil {
                let row = (BeaconTracingManager.shared.logs?.count ?? 0) - 1
                self?.tableView.insertRows(at: [IndexPath(row: row, section: 0)], with: .automatic)
            }
        }
        
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { [weak self] _ in
            self?.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    @objc func dismissController(sender:UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension LogsViewController:UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BeaconTracingManager.shared.logs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let log = BeaconTracingManager.shared.logs![indexPath.row]
        let type = BeaconTracingManager.shared.beacons?[log.beaconId]?.tracingType
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogCell", for: indexPath)
        cell.textLabel?.text = "\(log.beaconId.UUID) | \(log.beaconId.major) | \(log.beaconId.minor)\nLat: \(String(format:"%.5f",log.coordinate.latitude)) | Long: \(String(format:"%.5f", log.coordinate.longitude)) | Floor: \(String(describing: log.floor))"
        cell.detailTextLabel?.text = "\(Self.dateFormatter.string(from: log.date)) | \(type == .equipment ? "Equipment" : "Badge") | \(Int(log.time / 60.0)) min"
        cell.accessoryType = log.uploaded ? .checkmark : .none
        return cell
    }
}

