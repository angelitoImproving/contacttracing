//
//  PairingBeaconViewController.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 30/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit
import CoreLocation

class PairingBeaconViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var beacons:[(id:BeaconId, RSSI:Int, accuracy:Double)]?
    private var pairedBadge:BadgeBeacon?
    private var didRangeCallback:CallbackWrapper<BeaconTracingManager.RangeCallback>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        if let pairedBadge = BeaconTracingManager.shared.beacons?.first(where: { return ($0.value.beacon as? BadgeBeacon)?.userId == ContactTracing.shared.userId })?.value.beacon as? BadgeBeacon {
            self.pairedBadge = pairedBadge
        }
        else {
            self.getUnpairedBadges()
        }
    }
    
    private func getUnpairedBadges() {
        self.didRangeCallback = CallbackWrapper<BeaconTracingManager.RangeCallback>(callback: { [weak self] beacons in
            self?.beacons = beacons.map { (BeaconId(UUID: $0.proximityUUID.uuidString, major: $0.major.intValue, minor: $0.minor.intValue), $0.rssi, $0.accuracy )}
                .filter({ beacon in
                    
                    if let badgeBeacon = BeaconTracingManager.shared.beacons?[beacon.id]?.beacon as? BadgeBeacon {
                        if badgeBeacon.isReadyToPair && !badgeBeacon.isPaired {
                            return true
                        }
                    }
                    
                    return false
                })
                .sorted(by: { $0.accuracy < $1.accuracy })
            self?.tableView.reloadData()
        })
        BeaconTracingManager.shared.subscribeToDidRangeBeacons(callback: self.didRangeCallback)
    }
}

extension PairingBeaconViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pairedBadge != nil ? 1 : self.beacons?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let beacon = self.pairedBadge != nil ? (id:self.pairedBadge!.beaconId, RSSI:0, accuracy:0) : self.beacons![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PairingCell", for: indexPath) as! PairingBeaconTVCell
        cell.uuidLabel.text = self.pairedBadge?.beaconId.UUID ?? beacon.id.UUID
        cell.majorMinorLabel.text = "Major: \(self.pairedBadge?.beaconId.major ?? beacon.id.major) | Minor: \(self.pairedBadge?.beaconId.minor ?? beacon.id.minor)"
        
        if self.pairedBadge != nil {
            cell.distanceLabel.isHidden = true
            cell.isPairedSwitch.isOn = true
        }
        else {
            cell.distanceLabel.isHidden = false
            cell.distanceLabel.text = String(format: "Distance: %.4f meters", beacon.accuracy)
        }
        
        cell.pairStateChangeRequested = { [weak self] sender in
            
            if let index = self?.tableView.indexPath(for: sender)?.row,
                let beaconId = self?.beacons?[safe:index]?.id ?? self?.pairedBadge?.beaconId,
                let badge = BeaconTracingManager.shared.beacons?[beaconId]?.beacon as? BadgeBeacon {
                let message = self?.pairedBadge == nil ? "Do you want to pair badge:\nUUID: \(beacon.id.UUID)\nMajor: \(beacon.id.major)\nMinor: \(beacon.id.minor)" :
                "Are you sure you want to unpair this badge?"
                let alert = UIAlertController(title: "Badge Pairing", message: message, preferredStyle: .alert)
                let no = UIAlertAction(title: "No", style: .cancel, handler: { _ in
                    sender.isPairedSwitch.setOn(self?.pairedBadge != nil, animated: true)
                })
                let yes = UIAlertAction(title: "Yes", style: .default) { _ in
                    sender.isPairedSwitch.isHidden = true
                    sender.activityIndicatorView.startAnimating()
                    badge.isPaired = self?.pairedBadge == nil
                    badge.userId = self?.pairedBadge == nil ? ContactTracing.shared.userId : nil
                    let body = try? badge.jsonData()
                    NetworkingHelper.shared.request(url: ContactTracing.shared.baseUrl + Constants.Urls.BADGES, method: .put, headers: ["Content-Type":"application/json"], body: body, retryWaitingTime: 1, maxRetryAttempts: 5) { (data, response, _) in
                        
                        if let badge = data?.toObject(type: BadgeBeacon.self) {
                            BeaconTracingManager.shared.beacons?[beacon.id]?.beacon = badge
                            if self?.pairedBadge == nil {//Pairing
                                self?.pairedBadge = badge
                                BeaconTracingManager.shared.unsubscribeFromDidRangeBeacons(callback: self?.didRangeCallback)
                                self?.didRangeCallback = nil
                                self?.beacons = nil
                            }
                            else {//Unpairing
                                self?.pairedBadge = nil
                                self?.getUnpairedBadges()
                            }
                            
                            self?.tableView.reloadData()
                        }
                        else {
                            sender.isPairedSwitch.setOn(self?.pairedBadge != nil, animated: true)
                        }
                    }
                }
                alert.addAction(no)
                alert.addAction(yes)
                self?.present(alert, animated: true, completion: nil)
            }
            else {
                sender.isPairedSwitch.setOn(false, animated: true)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if self.pairedBadge == nil {
            return "Please keep your badge near to your phone and pair it by turning on the switch. Make sure the badge you are pairing has the same UUID, Major and Minor that your physical badge."
        }
        else {
            return "This is the badge paired to your user, to unpair, please turn off the switch."
        }
    }
}
