//
//  GridCVController.swift
//  ContactTracing
//
//  Created by Juan Pablo Rodriguez Medina on 02/08/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import UIKit

class GridCVController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    @IBOutlet weak var beaconTypeSegmentedControl: UISegmentedControl!
    
    private var equipmentRecords: [EquipmentRecord]?
    private var equipmentHeaders: [(header: String, width: CGFloat)] = [("Date", 100.0), ("UUID", 200.0), ("Major", 80.0), ("Minor", 80.0), ("Latitude", 100.0), ("Longitude", 100.0), ("Floor", 80.0), ("Unit Id", 80.0), ("Accuracy", 80.0)]
    
    private var badgesRecords: [BadgeRecord]?
    private var badgesHeaders: [(header: String, width: CGFloat)] = [("Date", 100.0), ("User A", 220.0), ("User B", 220.0), ("Latitude", 100.0), ("Longitude", 100.0), ("Floor", 80.0), ("Duration", 80.0)]
    
    private var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-YY\nHH:mm:ss"
        return dateFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.collectionViewLayout = CollectionViewGridLayout()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.isDirectionalLockEnabled = true
        
        self.refreshData()
    }
    
    private func getEquipment(completion: @escaping () -> Void) {
        NetworkingHelper.shared.request(url: ContactTracing.shared.baseUrl + Constants.Urls.EQUIPMENT_LOG, method: .get, headers: nil, body: nil) { (data, response, error) in
            self.equipmentRecords = data?.toObject(type: [EquipmentRecord].self)?.sorted(by: { (e1, e2) -> Bool in
                if let date1 = e1.dateTime, let date2 = e2.dateTime {
                    return date1 > date2
                }
                return false
            })
            completion()
        }
    }
    
    private func getBadges(completion: @escaping () -> Void) {
        NetworkingHelper.shared.request(url: ContactTracing.shared.baseUrl + Constants.Urls.BADGE_LOG, method: .get, headers: nil, body: nil) { (data, response, error) in
            self.badgesRecords = data?.toObject(type: [BadgeRecord].self)?.sorted(by: { (e1, e2) -> Bool in
                if let date1 = e1.dateTime, let date2 = e2.dateTime {
                    return date1 > date2
                }
                return false
            })
            completion()
        }
    }
    
    private func refreshData() {
        
        let group = DispatchGroup()
        
        group.enter()
        self.getEquipment(completion: { group.leave() })
        group.enter()
        self.getBadges(completion: { group.leave() })
        
        group.notify(queue: .main, execute: {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-YYYY HH:mm:ss"
            self.lastUpdatedLabel.text = "Last updated: " + dateFormatter.string(from: Date())
            self.collectionView.reloadData()
        })
    }
    
    @IBAction func refresh(_ sender: Any) {
        self.refreshData()
    }
    
    @IBAction func beaconTypeDidChange(_ sender: Any) {
        self.collectionView.reloadData()
        self.collectionView.contentOffset = .zero
    }
    
    @IBAction func done(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension GridCVController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let selectedType = self.beaconTypeSegmentedControl.selectedSegmentIndex
        return (selectedType == 0 ? (self.equipmentRecords?.count ?? 0)
            : (self.badgesRecords?.count ?? 0)) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let selectedType = self.beaconTypeSegmentedControl.selectedSegmentIndex
        return selectedType == 0 ? self.equipmentHeaders.count : self.badgesHeaders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCell", for: indexPath) as! GridCVCell
        
        if indexPath.section == 0 {
            if self.beaconTypeSegmentedControl.selectedSegmentIndex == 0 {
                cell.label.text = self.equipmentHeaders[indexPath.item].header
            }
            else {
                cell.label.text = self.badgesHeaders[indexPath.item].header
            }
            cell.label.font = UIFont.boldSystemFont(ofSize: 15.0)
        }
        else {
            cell.label.font = UIFont.systemFont(ofSize: 15.0)
            if self.beaconTypeSegmentedControl.selectedSegmentIndex == 0 {
                let record = self.equipmentRecords![indexPath.section - 1]
                switch indexPath.item {
                    case 0:
                        if let dateTime = record.dateTime {
                            cell.label.text = self.dateFormatter.string(from: dateTime)
                        }
                    case 1: cell.label.text = record.UUID
                    case 2: cell.label.text = "\(record.major)"
                    case 3: cell.label.text = "\(record.minor)"
                    case 4: cell.label.text = String(format: "%.6f",record.location.point[1])
                    case 5: cell.label.text = String(format: "%.6f",record.location.point[0])
                    case 6: cell.label.text = record.floor != nil ? "\(record.floor!)" : "-"
                    case 7: cell.label.text = record.unitId != nil ? record.unitId : "-"
                    case 8: cell.label.text = record.accuracy != nil ? String(format: "%.4f", record.accuracy!) : "-1"
                    default: break
                }
            }
            else {
                let record = self.badgesRecords![indexPath.section - 1]
                switch indexPath.item {
                    case 0:
                        if let dateTime = record.dateTime {
                            cell.label.text = self.dateFormatter.string(from: dateTime)
                        }
                    case 1: cell.label.text = record.userId
                    case 2: cell.label.text = record.contactUserId
                    case 3: cell.label.text = String(format: "%.6f",record.location.point[1])
                    case 4: cell.label.text = String(format: "%.6f",record.location.point[0])
                    case 5: cell.label.text = record.floor != nil ? "\(record.floor!)" : "-"
                    case 6: cell.label.text = record.contactTime != nil ? "\(record.contactTime!)" : "-"
                    default: break
                }
            }
        }
        
        return cell
    }
}

extension GridCVController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.item == 0 || indexPath.section == 0 {
            cell.contentView.backgroundColor = UIColor.systemGray5
        }
        else if indexPath.section % 2 == 0 {
            cell.contentView.backgroundColor = UIColor.secondarySystemBackground
        }
        else {
            cell.contentView.backgroundColor = UIColor.systemBackground
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let selectedType = self.beaconTypeSegmentedControl.selectedSegmentIndex
        let header = selectedType == 0 ? self.equipmentHeaders[indexPath.item] : self.badgesHeaders[indexPath.item]
        return CGSize(width: header.width, height: 45.0)
    }
}
