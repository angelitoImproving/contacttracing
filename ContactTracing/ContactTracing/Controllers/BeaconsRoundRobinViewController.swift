//
//  BeaconsRoundRobinViewController.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 20/05/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class BeaconsRoundRobinViewController: UITableViewController {
    
    @IBOutlet weak var maxConcurrentRegionsStepper: UIStepper!
    @IBOutlet weak var maxConcurrentRegionsValueLabel: UILabel!
    @IBOutlet weak var isEnabledSwitch: UISwitch!
    @IBOutlet weak var roundRobinTimeValueLabel: UILabel!
    @IBOutlet weak var roundRobinTimeStepper: UIStepper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isEnabledSwitch.isOn = BeaconTracingManager.shared.isRoundRobinEnabled
        
        self.maxConcurrentRegionsStepper.value = Double(BeaconTracingManager.shared.maxSimultaneousRangingCount)
        self.maxConcurrentRegionsStepper.minimumValue = 1
        self.maxConcurrentRegionsValueLabel.text = "\(Int(self.maxConcurrentRegionsStepper.value))"
        
        self.roundRobinTimeStepper.value = BeaconTracingManager.shared.roundRobinTime
        self.roundRobinTimeStepper.minimumValue = 10
        self.roundRobinTimeStepper.stepValue = 10
        self.roundRobinTimeValueLabel.text = "\(Int(self.roundRobinTimeStepper.value))"
    }
    
    @IBAction func isEnabledValueChanged(_ sender: UISwitch) {
        BeaconTracingManager.shared.isRoundRobinEnabled = sender.isOn
    }
    
    @IBAction func maxConcurrentRegionsValueChanged(_ sender: UIStepper) {
        BeaconTracingManager.shared.maxSimultaneousRangingCount = Int(sender.value)
        self.maxConcurrentRegionsValueLabel.text = "\(Int(sender.value))"
    }
    
    @IBAction func roundRobinTimeChanged(_ sender: UIStepper) {
        BeaconTracingManager.shared.roundRobinTime = sender.value
        self.roundRobinTimeValueLabel.text = "\(Int(sender.value))"
    }
}
