//
//  BeaconTracingViewController.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 25/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class BeaconTracingViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var monitoringBarButtonItem: UIBarButtonItem!
    
    
    private var indexPaths = [BeaconId:IndexPath]()
    private var values =  [BeaconId:(Int, Double)]()
    
    private let numberFormatter:NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 4
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Dismiss button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissController(sender:)))
        
        //Monitoring button
        self.monitoringBarButtonItem.tintColor = BeaconTracingManager.shared.isMonitoring ? UIColor(named: "ranging-green") : UIColor.red
        
        //TableView
        self.tableView.dataSource = self
        self.tableView.delegate = self
        let bundle = Bundle(identifier: "com.navv-systems.ContactTracing")
        self.tableView.register(UINib(nibName: "RegionTVHeader", bundle: bundle), forHeaderFooterViewReuseIdentifier: "RegionHeader")
        
        //Callback for every range event
        BeaconTracingManager.shared.subscribeToDidRangeBeacons(callback: CallbackWrapper<BeaconTracingManager.RangeCallback> { [weak self] beacons in
            for clbeacon in beacons {
                let beacon = BeaconId(UUID: clbeacon.uuid.uuidString, major: clbeacon.major.intValue, minor: clbeacon.minor.intValue)
                self?.values[beacon] = (clbeacon.rssi, clbeacon.accuracy)
                if UIApplication.shared.applicationState == .active && self?.viewIfLoaded?.window != nil {
                    if let indexPath = self?.indexPaths[beacon],
                        let cell = self?.tableView.cellForRow(at: indexPath) as? BeaconTVCell{
                        cell.rssiLabel.text = "\(clbeacon.rssi)"
                        cell.accuracyLabel.text = self?.numberFormatter.string(from: NSNumber(value: clbeacon.accuracy))
                    }
                }
            }
        })
        
        //Callback for did start ranging region
        BeaconTracingManager.shared.didStartRangingRegion = { [weak self] (uuid, index) in
            if UIApplication.shared.applicationState == .active && self?.viewIfLoaded?.window != nil {
                if let header = self?.tableView.headerView(forSection: index) as? RegionTVHeader,
                    let status = BeaconTracingManager.shared.regions?[index].status {
                    header.setStatus(status.displayText, withColor: status.displayColor ?? UIColor.black, animating: true)
                }
            }
        }
        
        //Callback for did stop ranging region
        BeaconTracingManager.shared.didStopRangingRegion = { [weak self] (uuid, index) in
            if UIApplication.shared.applicationState == .active && self?.viewIfLoaded?.window != nil {
                if let header = self?.tableView.headerView(forSection: index) as? RegionTVHeader,
                    let status = BeaconTracingManager.shared.regions?[index].status {
                    header.setStatus(status.displayText, withColor: status.displayColor ?? UIColor.black, animating: false)
                }
            }
        }
        
        //Callback for when the manager got data from server
        BeaconTracingManager.shared.didFinishGettingBeacons = { [weak self] in
            self?.tableView.reloadData()
        }
        
        //Callback for when the manager starts monitoring regions
        BeaconTracingManager.shared.didStartMonitoringRegions = { [weak self] in
            self?.monitoringBarButtonItem.tintColor = BeaconTracingManager.shared.isMonitoring ? UIColor(named: "ranging-green") : UIColor.red
            self?.tableView.reloadData()
        }
        
        //Callback for when the manager stops monitoring regions
        BeaconTracingManager.shared.didStopMonitoringRegions = { [weak self] in
            self?.monitoringBarButtonItem.tintColor = BeaconTracingManager.shared.isMonitoring ? UIColor(named: "ranging-green") : UIColor.red
            self?.tableView.reloadData()
        }
        
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { [weak self] _ in
            self?.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    @objc func dismissController(sender:UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func rangeBeacons(_ sender: UIBarButtonItem) {
        if BeaconTracingManager.shared.isMonitoring {
            BeaconTracingManager.shared.stopMonitoring()
        }
        else {
            BeaconTracingManager.shared.startMonitoring()
        }
    }
    
    @IBAction func refreshBeacons(_ sender: UIBarButtonItem) {
        BeaconTracingManager.shared.stopMonitoring()
        BeaconTracingManager.shared.getBeacons(shouldMonitor: false)
    }
}

extension BeaconTracingViewController:UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return BeaconTracingManager.shared.regions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BeaconTracingManager.shared.regions?[section].beacons.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BeaconCell", for: indexPath) as! BeaconTVCell
        
        guard let beaconId = BeaconTracingManager.shared.regions, beaconId.indices.contains(indexPath.section) else {
            return cell
        }
        
        let beaconIdUnw = beaconId[indexPath.section].beacons[indexPath.row].beaconId
        let beaconStatus = BeaconTracingManager.shared.beacons?[beaconIdUnw]
        
        self.indexPaths[beaconIdUnw] = indexPath
        
        
        switch beaconStatus?.tracingType {
            case .equipment:
                cell.typeLabel.text = "Equipment"
            case .badge:
                let userId = (beaconStatus?.beacon as? BadgeBeacon)?.userId
                cell.typeLabel.text = "Badge \(userId == nil ? "(Not paired)" : userId == ContactTracing.shared.userId ? "(My badge)" : "")"
            default:
                cell.typeLabel.text = "Unknown"
        }
        
        cell.majorLabel.text = "\(beaconIdUnw.major)"
        cell.minorLabel.text = "\(beaconIdUnw.minor)"
        if let values = self.values[beaconIdUnw] {
            cell.rssiLabel.text = "\(values.0)"
            cell.accuracyLabel.text = self.numberFormatter.string(from: NSNumber(value: values.1))
        } else {
            cell.rssiLabel.text = "-1"
            cell.accuracyLabel.text = "-1"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "RegionHeader") as? RegionTVHeader
        
        guard let region = BeaconTracingManager.shared.regions, region.indices.contains(section) else {
            return header
        }
                
        header?.uuidLabel.text = region[section].UUID
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? RegionTVHeader {
            guard let status = BeaconTracingManager.shared.regions, status.indices.contains(section) else {
                return
            }
            
            header.setStatus(status[section].status.displayText, withColor: status[section].status.displayColor ?? UIColor.black, animating: status[section].status == .ranging || status[section].status == .monitored)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70.0
    }
}
