//
//  BeaconsSettingsViewController.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 20/05/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class BeaconsSettingsViewController: UITableViewController {
    
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 1 {
            self.updateLoggingTimes()
        }
    }
    
    private func updateLoggingTimes() {
        let alertController = UIAlertController(title: "Badges Logging Times", message: "Please set the logging times for badges. Enter the numbers in minutes, separated by commas.", preferredStyle: .alert)
        alertController.addTextField { (textField) in
            let tiersText = BeaconTracingManager.shared.badgeLogTimeTiers.reduce(String(), { (text, value) in
                return text + "\(text.isEmpty ? "" : ",")\(Int(value / 60))"
            })
            textField.text = tiersText
        }
        let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
            if let text = alertController.textFields?.first?.text {
                let strValues = text.trimmingCharacters(in: .whitespaces).components(separatedBy: ",")
                let values = strValues.reduce(into: [Double]()) { (accumulated, strVal) in
                    if let val = Double(strVal) {
                       accumulated.append(val * 60)
                    }
                }
                BeaconTracingManager.shared.badgeLogTimeTiers = values
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
