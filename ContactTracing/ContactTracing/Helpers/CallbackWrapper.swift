//
//  CallbackWrapper.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 04/05/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation

public class CallbackWrapper<T> {
    
    public let id = UUID()
    public let callback:T
    
    public init(callback: T) {
        self.callback = callback
    }
}
