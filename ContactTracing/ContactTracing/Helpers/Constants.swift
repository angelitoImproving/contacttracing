//
//  Constants.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 08/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation

struct Constants {
    struct Urls {
        static let EQUIPMENT = "equipment"
        static let BADGES = "badges"
        static let EQUIPMENT_LOG = "equipment_crowdsources"
        static let BADGE_LOG = "contact_events"
    }
}
