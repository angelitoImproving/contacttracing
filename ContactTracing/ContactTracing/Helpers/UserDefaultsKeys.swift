//
//  UserDefaultsKeys.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 03/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation

enum UserDefaultsKeys: String {
    case isRoundRobinEnabled = "is_round_robin_enabled"
    case roundRobinMaxSimultaneousRangingCount = "round_robin_max_simultaneous_ranging_count"
    case roundRobinTime = "round_robin_time"
    case badgeLogTimeTiers = "badge_log_time_tiers"
    case mutedContactUsers = "muted_contact_users"
    case shouldDisplayContactAlerts = "should_display_contact_alerts"
}
