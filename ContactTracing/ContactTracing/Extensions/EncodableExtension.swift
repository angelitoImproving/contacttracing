//
//  Encodable.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 08/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation

extension Encodable {
    func jsonData() throws -> Data {
        let encoder = JSONEncoder()
        return try encoder.encode(self)
    }
}
