//
//  DataExtension.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 08/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation

extension Data {
    func toObject<T:Decodable>(type:T.Type) -> T? {
        
        do {
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode(T.self, from: self)
            return jsonData
        }
        catch let err {
            print("Error parsing json: \(err.localizedDescription)")
            return nil
        }
    }
}
