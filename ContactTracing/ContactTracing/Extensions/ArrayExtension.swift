//
//  ArrayExtension.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 08/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation

extension Array {
    subscript(safe index:Index) -> Element? {
        if index >= 0 && index < self.count {
            return self[index]
        }
        return nil
    }
}
