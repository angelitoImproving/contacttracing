//
//  UserDefaultsExtension.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 08/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    //MARK: Functions
    
    func array<Element>(forKey key: String) -> Array<Element>? {
        return self.array(forKey: key) as? Array<Element>
    }
    
    func set<Element>(value:Set<Element>?, forKey key:String) {
        if let set = value {
            self.set(Array(set), forKey: key)
        }
        else {
            self.set(nil, forKey: key)
        }
    }
    
    func set<Element>(forKey key:String) -> Set<Element>? {
        if let array = self.array(forKey: key) as? Array<Element> {
            return Set(array)
        }
        return nil
    }
}
