//
//  Region.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 25/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import UIKit

class Region {
    
    enum Status {
        case ranging
        case roundRobin
        case monitored
        case notMonitored
        
        var displayText:String {
            switch self {
                case .ranging: return "Ranging"
                case .roundRobin: return "Round-Robin"
                case .monitored: return "Monitoring"
                case .notMonitored: return "Not Monitored"
            }
        }
        
        var displayColor:UIColor? {
            switch self {
                case .ranging: return UIColor(named: "ranging-green")
            case .roundRobin: return UIColor(named: "round-robin-blue")
                case .monitored: return UIColor(named: "monitoring-orange")
                case .notMonitored: return UIColor.gray
            }
        }
    }
    
    let UUID:String
    var beacons:[Beacon] = [Beacon]()
    var status:Status = .notMonitored
    
    init(uuid:String) {
        self.UUID = uuid
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.UUID)
    }
}
