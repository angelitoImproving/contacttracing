//
//  ContactLog.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 25/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation
import CoreLocation

class ContactLog {
    let beaconId:BeaconId
    let date:Date
    let time:Double
    let coordinate:CLLocationCoordinate2D
    let floor:Int?
    let uploaded:Bool
    
    init(beaconId:BeaconId, date:Date, time:Double, coordinate:CLLocationCoordinate2D, floor:Int?, uploaded:Bool) {
        self.beaconId = beaconId
        self.date = date
        self.time = time
        self.coordinate = coordinate
        self.floor = floor
        self.uploaded = uploaded
    }
}
