//
//  KDTree+CustomStringConvertible.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 27/12/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation

extension KDTree : CustomStringConvertible, CustomDebugStringConvertible {
    
    /// A textual representation of `self`.
    public var description: String {
        switch self {
        case .leaf:
            return "🍁"
        case let .node(left, value, _, right):
            return left.description + String(describing: value) + right.description
        }
    }
    
    /// A textual representation of `self`, suitable for debugging.
    public var debugDescription: String {
        switch self {
        case .leaf:
            return "🍁"
        case let .node(left, value, _, right):
            return "{\(left.debugDescription) \(String(describing: value)) \(right.debugDescription)}"
        }
    }
}
