//
//  KDTreeGrowing.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 27/12/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation

public protocol KDTreePoint: Equatable {
    static var dimensions: Int { get }
    func kdDimension(_ dimension: Int) -> Double
    func squaredDistance(to otherPoint: Self) -> Double
}
