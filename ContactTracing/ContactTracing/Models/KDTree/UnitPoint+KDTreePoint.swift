//
//  CGPoint+KDTreeGrowing.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 27/12/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

@available(iOS 13.0, *)
struct UnitPoint {
    let identifier: UUID?
    let title: String?
    let category: String?
    let coordinate: CLLocationCoordinate2D
    let geometry: [MKShape & MKGeoJSONObject]?
}

@available(iOS 13.0, *)
extension UnitPoint: KDTreePoint {
    
    public static var dimensions = 2
    
    public func kdDimension(_ dimension: Int) -> Double {
        
        return dimension == 0 ? Double(self.coordinate.latitude) : Double(self.coordinate.longitude)
    }
    
    public func squaredDistance(to otherPoint: UnitPoint) -> Double {
        
        let x = self.coordinate.latitude - otherPoint.coordinate.latitude
        let y = self.coordinate.longitude - otherPoint.coordinate.longitude
        return Double(x*x + y*y)
    }
    
    public static func == (lhs: UnitPoint, rhs: UnitPoint) -> Bool {
        return lhs.coordinate.longitude == rhs.coordinate.longitude &&
            lhs.coordinate.latitude == rhs.coordinate.latitude
    }
}
