//
//  Location.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 08/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation

struct Location: Codable {
    let point:[Double]
    
    enum CodingKeys: String, CodingKey{
        case point = "Point"
    }
    
    init(latitude:Double, longitude:Double) {
        self.point = [longitude, latitude]
    }
}
