//
//  BadgeRecord.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 09/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation
import CoreLocation

struct BadgeRecord:Codable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case userId
        case dateTime
        case location
        case floor
        case contactUserId
        case contactTime
    }
    
    var id:String?
    let userId:String
    let dateTime:Date?
    let location:Location
    let floor:Int?
    let contactUserId:String
    let contactTime:Int?
    
    private var isoDateFormatter: ISO8601DateFormatter = {
       let isoFormatter = ISO8601DateFormatter()
        isoFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        return isoFormatter
    }()
    
    init(id:String?, userId:String, dateTime:Date, location:CLLocationCoordinate2D, floor:Int?, contactUserId:String, contactTime:Int) {
        self.id = id
        self.userId = userId
        self.dateTime = dateTime
        self.location = Location(latitude: location.latitude, longitude: location.longitude)
        self.floor = floor
        self.contactUserId = contactUserId
        self.contactTime = contactTime
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.userId = try container.decode(String.self, forKey: .userId)
        let dateTimeStr = try container.decode(String.self, forKey: .dateTime)
        self.dateTime = self.isoDateFormatter.date(from: dateTimeStr)
        self.location = try container.decode(Location.self, forKey: .location)
        self.floor = try container.decodeIfPresent(Int.self, forKey: .floor)
        self.contactUserId = try container.decode(String.self, forKey: .contactUserId)
        self.contactTime = try container.decodeIfPresent(Int.self, forKey: .contactTime)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if self.id != nil {
            try container.encode(self.id, forKey: .id)
        }
        try container.encode(self.userId, forKey: .userId)
        if let dateTime = self.dateTime {
            let isoDateStr = self.isoDateFormatter.string(from: dateTime)
            try container.encode(isoDateStr, forKey: .dateTime)
        }
        try container.encode(self.location, forKey: .location)
        if let floor = self.floor {
            try container.encode(floor, forKey: .floor)
        }
        try container.encode(self.contactUserId, forKey: .contactUserId)
        if let contactTime = self.contactTime {
            try container.encode(contactTime, forKey: .contactTime)
        }
    }
}

