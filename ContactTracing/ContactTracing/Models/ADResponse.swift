//
//  ADResponse.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 26/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation

struct ADResponse: Codable {
    
    struct Session: Codable {
        let id: String
        let email: String
        let firstName: String
        let lastName: String
        let ttl: Int
        let created: String
        let userId: String
        let jobId: String
        let campusId: String
    }
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case session = "res"
    }
    
    let success: Bool
    let session: Session
}
