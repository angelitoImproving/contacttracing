/*
See LICENSE folder for this sample’s licensing information.

Abstract:
The decoded representation of an IMDF Level feature type.
*/

import Foundation

@available(iOS 13.0, *)
class Level: Feature<Level.Properties> {
    struct Properties: Codable {
        let ordinal: Int
    }
}

// For more information about this class, see: https://register.apple.com/resources/imdf/Level/
