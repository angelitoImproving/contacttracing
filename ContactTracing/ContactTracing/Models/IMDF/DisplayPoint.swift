//
//  DisplayPoint.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 18/11/19.
//  Copyright © 2019 Paul Zieske. All rights reserved.
//

import Foundation
import CoreLocation

struct DisplayPoint:Codable {
    
    enum CodingKeys:String, CodingKey {
        case coordinates
    }
    
    let coordinates:CLLocationCoordinate2D
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let coordinatesArray = try values.decode([Double].self, forKey: .coordinates)
        self.coordinates = CLLocationCoordinate2D(latitude: coordinatesArray[1], longitude: coordinatesArray[0])
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        let coordinates = [self.coordinates.longitude, self.coordinates.latitude]
        try container.encode(coordinates, forKey: .coordinates)
    }
}
