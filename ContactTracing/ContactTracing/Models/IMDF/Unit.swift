/*
See LICENSE folder for this sample’s licensing information.

Abstract:
The decoded representation of an IMDF Unit feature type.
*/

import Foundation
import MapKit

@available(iOS 13.0, *)
class Unit: Feature<Unit.Properties> {
    
    struct Properties: Codable {
        let category: String
        let levelId: UUID
        let name: LocalizedName?
        let altName: LocalizedName?
        let displayPoint: DisplayPoint?
    }
}


// For more information about this class, see: https://register.apple.com/resources/imdf/Unit/
