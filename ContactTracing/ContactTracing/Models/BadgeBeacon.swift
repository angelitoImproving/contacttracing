//
//  BadgeBeacon.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 27/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation

class BadgeBeacon: Beacon, Codable {
    
    var id:String?
    var beaconId: BeaconId
    var isReadyToPair:Bool
    var isPaired:Bool
    var userId:String?
    
    enum CodingKeys:String, CodingKey {
        case id
        case UUID
        case major
        case minor
        case isReadyToPair = "IsReadyToPair"
        case isPaired = "IsPaired"
        case userId
    }
    
    init(id:String?, uuid:String, major:Int, minor:Int, isReadyToPair:Bool, isPaired:Bool, userId:String) {
        self.id = id
        self.beaconId = BeaconId(UUID: uuid, major: major, minor: minor)
        self.isReadyToPair = isReadyToPair
        self.isPaired = isPaired
        self.userId = userId
    }
    
    required init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        let uuid = try container.decode(String.self, forKey: .UUID)
        let major = try container.decode(Int.self, forKey: .major)
        let minor = try container.decode(Int.self, forKey: .minor)
        self.beaconId = BeaconId(UUID: uuid, major: major, minor: minor)
        self.isReadyToPair = try container.decode(Bool.self, forKey: .isReadyToPair)
        self.isPaired = try container.decode(Bool.self, forKey: .isPaired)
        self.userId = try container.decodeIfPresent(String.self, forKey: .userId)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if self.id != nil {
            try container.encode(self.id, forKey: .id)
        }
        try container.encode(self.beaconId.UUID, forKey: .UUID)
        try container.encode(self.beaconId.major, forKey: .major)
        try container.encode(self.beaconId.minor, forKey: .minor)
        try container.encode(self.isReadyToPair, forKey: .isReadyToPair)
        try container.encode(self.isPaired, forKey: .isPaired)
        try container.encode(self.userId, forKey: .userId)
    }
}
