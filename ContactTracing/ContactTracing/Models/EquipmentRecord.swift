//
//  EquipmentRecord.swift
//  SocialDistancingAssistant
//
//  Created by Juan Pablo Rodriguez Medina on 08/06/20.
//  Copyright © 2020 Juan Pablo Rodriguez Medina. All rights reserved.
//

import Foundation
import CoreLocation

struct EquipmentRecord: Codable {
    var id:String?
    let UUID:String
    let major:Int
    let minor:Int
    let location:Location
    let floor:Int?
    let accuracy:Double?
    let unitId: String?
    let dateTime:Date?
    let deviceId:String
    
    private var isoDateFormatter: ISO8601DateFormatter = {
       let isoFormatter = ISO8601DateFormatter()
        isoFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        return isoFormatter
    }()
    
    enum CodingKeys: String, CodingKey {
        case id
        case uuid = "UUID"
        case major
        case minor
        case location
        case floor
        case accuracy
        case unitId
        case dateTime = "datetime"
        case deviceId = "device_id"
    }
    
    init(UUID:String, major:Int, minor:Int, location:CLLocationCoordinate2D, floor:Int?, accuracy:Double, unitId:String, dateTime:Date, deviceId:String) {
        self.UUID = UUID
        self.major = major
        self.minor = minor
        self.location = Location(latitude: location.latitude, longitude: location.longitude)
        self.floor = floor
        self.accuracy = accuracy
        self.unitId = unitId
        self.dateTime = dateTime
        self.deviceId = deviceId
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.UUID = try container.decode(String.self, forKey: .uuid)
        self.major = try container.decode(Int.self, forKey: .major)
        self.minor = try container.decode(Int.self, forKey: .minor)
        self.location = try container.decode(Location.self, forKey: .location)
        self.floor = try container.decodeIfPresent(Int.self, forKey: .floor)
        self.accuracy = try container.decodeIfPresent(Double.self, forKey: .accuracy)
        self.unitId = try container.decodeIfPresent(String.self, forKey: .unitId)
        let dateTimeStr = try container.decode(String.self, forKey: .dateTime)
        self.dateTime = self.isoDateFormatter.date(from: dateTimeStr)
        self.deviceId = try container.decode(String.self, forKey: .deviceId)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if self.id != nil {
            try container.encode(self.id, forKey: .id)
        }
        try container.encode(self.UUID, forKey: .uuid)
        try container.encode(self.major, forKey: .major)
        try container.encode(self.minor, forKey: .minor)
        try container.encode(self.location, forKey: .location)
        try container.encode(self.floor, forKey: .floor)
        try container.encode(self.accuracy, forKey: .accuracy)
        try container.encode(self.unitId, forKey: .unitId)
        if let dateTime = self.dateTime {
            let isoDateStr = self.isoDateFormatter.string(from: dateTime)
            try container.encode(isoDateStr, forKey: .dateTime)
        }
        try container.encode(self.deviceId, forKey: .deviceId)
    }
}
