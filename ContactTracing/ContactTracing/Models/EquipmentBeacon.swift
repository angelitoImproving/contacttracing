//
//  Equipment.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 20/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation

class EquipmentBeacon: Beacon, Decodable {
    var beaconId: BeaconId
    let deviceCategory:String
    let assetTag:String
    let model:String
    
    enum CodingKeys:String, CodingKey {
        case UUID
        case major
        case minor
        case deviceCategory = "device_category"
        case assetTag = "asset_tag"
        case model
    }
    
    required init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let uuid = try container.decode(String.self, forKey: .UUID)
        let major = try container.decode(Int.self, forKey: .major)
        let minor = try container.decode(Int.self, forKey: .minor)
        self.beaconId = BeaconId(UUID: uuid, major: major, minor: minor)
        self.deviceCategory = try container.decode(String.self, forKey: .deviceCategory)
        self.assetTag = try container.decode(String.self, forKey: .assetTag)
        self.model = try container.decode(String.self, forKey: .model)
    }
}
