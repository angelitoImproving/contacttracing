//
//  BeaconId.swift
//  NovaTrack
//
//  Created by Juan Pablo Rodriguez Medina on 27/04/20.
//  Copyright © 2020 Paul Zieske. All rights reserved.
//

import Foundation

protocol Beacon {
    var beaconId:BeaconId { get }
}

public struct BeaconId: Codable, Hashable, CustomStringConvertible {
    
    public var description: String {
        return "\(self.UUID) | \(self.major) | \(self.minor)"
    }
    
    public let UUID:String
    public let major:Int
    public let minor:Int
    
    public init(UUID: String, major: Int, minor: Int) {
        self.UUID = UUID.uppercased()
        self.major = major
        self.minor = minor
    }
}
